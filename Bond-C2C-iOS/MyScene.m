//
//  MyScene.m
//  Bond-C2C-iOS
//
//  Created by Timothy Hart on 4/2/14.
//  Copyright (c) 2014 Timothy Hart. All rights reserved.
//

#import "MyScene.h"
#import "DrNoScene.h"

@implementation MyScene

//Create a few static string to use as category names for parts of the physics system
static NSString *bondCategoryName = @"bond";
static NSString *groundCategoryName = @"ground";

//Create a few static int constants to use at the collision bitmasks of the physics system
static const uint32_t bondCategory  = 0x1 << 0;
static const uint32_t groundCategory = 0x1 << 1;

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        //Using a background color for now.
        //TODO this will have to be a sprite on a certain layer that you can load through the texture atlas
        self.backgroundColor = [SKColor colorWithRed:0.15 green:0.15 blue:0.15 alpha:1.0];
        
        //Temporary post sprite
        SKSpriteNode *poster = [SKSpriteNode spriteNodeWithImageNamed:@"C2C-logo"];
        poster.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height /2);
        [self addChild:poster];
        
        //Create the sprite for bond, and place him on the left side of the screen
        self.bond = [SKSpriteNode spriteNodeWithImageNamed:@"stand"];
        self.bond.name = bondCategoryName;
        self.bond.position = CGPointMake(10, 100);
        [self addChild:self.bond];
        
        //By default, bond should not be crouching. You have the BOOL to keep bond crouching, but he will stand automatically when set to yes in the update method. You also use this in the hack for the physics body adjustment.
        self.isBondCrouching = NO;
        
        //Add a physics body for the bond sprite
        self.bond.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.bond.frame.size];
        self.bond.physicsBody.friction = 0.0f;
        self.bond.physicsBody.restitution = 1.0f;
        self.bond.physicsBody.linearDamping = 0.0f;
        self.bond.physicsBody.allowsRotation = NO;
        
        
        //By defaul the level is "marked" as not beaten. If bond "beats" the level (whatever) that means, then it is marked as YES and the level is beaten. The check for this is in the update: method.
        self.hasBeatenLevel = NO;
        
        //Set up the bond run animation frames
        NSMutableArray *runFrames = [NSMutableArray array];
        SKTextureAtlas *bondAnimatedAtlas = [SKTextureAtlas atlasNamed:@"bondImages"];
        for (int i = 1; i <= 8; i++) {
            NSString *textureName = [NSString stringWithFormat:@"run%d", i];
            SKTexture *temp = [bondAnimatedAtlas textureNamed:textureName];
            [runFrames addObject:temp];
        }
        self.bondRunFrames = runFrames;
        
        //Set up the left bond run frames.
        NSMutableArray *leftRunFrames = [NSMutableArray array];
        for (int i = 1; i <= 8; i++) {
            NSString *textureName = [NSString stringWithFormat:@"leftrun%d", i];
            SKTexture *temp = [bondAnimatedAtlas textureNamed:textureName];
            [leftRunFrames addObject:temp];
        }
        self.leftBondRunFrames = leftRunFrames;
        
        self.currentRunFrame = 1;
        
        //Setup the bond jump animation frames
        NSMutableArray *jumpFrames = [NSMutableArray array];
        for (int i = 1; i <= 3; i++){
            NSString *textureName = [NSString stringWithFormat:@"jump%d", i];
            SKTexture *temp = [bondAnimatedAtlas textureNamed:textureName];
            [jumpFrames addObject:temp];
        }
        self.bondJumpFrames = jumpFrames;
        
        //Setup the left bond jump animation frames
        NSMutableArray *leftJumpFrames = [NSMutableArray array];
        for (int i = 1; i <= 3; i++) {
            NSString *textureName = [NSString stringWithFormat:@"leftjump%d", i];
            SKTexture *temp = [bondAnimatedAtlas textureNamed:textureName];
            [leftJumpFrames addObject:temp];
        }
        self.leftBondJumpFrames = leftJumpFrames;
        
        self.currentJumpFrame = 1;
        
        //Setup the bond crouch animation frames
        NSMutableArray *crouchFrames = [NSMutableArray array];
        for (int i = 1; i <= 3; i++){
            NSString *textureName = [NSString stringWithFormat:@"crouch%d", i];
            SKTexture *temp = [bondAnimatedAtlas textureNamed:textureName];
            [crouchFrames addObject:temp];
        }
        self.bondCrouchFrames = crouchFrames;
        
        //Setup the left bond crouch animation frames
        NSMutableArray *leftCrouchFrames = [NSMutableArray array];
        for (int i = 1; i <= 3; i++) {
            NSString *textureName = [NSString stringWithFormat:@"leftcrouch%d", i];
            SKTexture *temp = [bondAnimatedAtlas textureNamed:textureName];
            [leftCrouchFrames addObject:temp];
        }
        self.leftBondCrouchFrames = leftCrouchFrames;
        
        self.currentCrouchFrame = 1;
        
        //Setup the physics world
        self.physicsWorld.gravity = CGVectorMake(0.0f, -3.0f);
        // 1 Create an physics body that borders the screen
        SKPhysicsBody* borderBody = [SKPhysicsBody bodyWithEdgeFromPoint:CGPointMake(0, 10) toPoint:CGPointMake(1024, 10)];
        // 2 Set physicsBody of scene to borderBody
        self.physicsBody = borderBody;
        // 3 Set the friction of that physicsBody to 0
        self.physicsBody.friction = 0.0f;
        
        SKSpriteNode *testNode = [SKSpriteNode spriteNodeWithColor:[SKColor whiteColor] size:CGSizeMake(50, 50)];
        testNode.position = CGPointMake(500, 50);
        SKPhysicsBody *test = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(50, 50)];
        test.allowsRotation = NO;
        testNode.physicsBody = test;
        test.dynamic = NO;
        [self addChild:testNode];
        
    
    }
    return self;
}

-(void)didMoveToView:(SKView *)view {
    self.swipeUpGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(jump)];
    self.swipeUpGesture.direction = UISwipeGestureRecognizerDirectionUp;
    [view addGestureRecognizer:self.swipeUpGesture];
    
    self.swipeDownGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(crouch)];
    self.swipeDownGesture.direction = UISwipeGestureRecognizerDirectionDown;
    [view addGestureRecognizer:self.swipeDownGesture];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        if (location.x > 512) { //Touch is on the right side of the screen
            self.isBondCrouching = NO;
            self.isBondFacingLeft = NO;
            self.bond.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.bond.frame.size];
            [self.bond runAction:[SKAction moveByX:20 y:0 duration:.2]];
            [self.bond setTexture:[self.bondRunFrames objectAtIndex:self.currentRunFrame]];
            self.currentRunFrame = self.currentRunFrame + 1;
            if (self.currentRunFrame == 8) self.currentRunFrame = 1;
        } else if (location.x < 512) { //Touch is on the left side of the screen
            self.isBondCrouching = NO;
            self.isBondFacingLeft = YES;
            self.bond.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.bond.frame.size];
            [self.bond runAction:[SKAction moveByX:-20 y:0 duration:.2]];
            [self.bond setTexture:[self.leftBondRunFrames objectAtIndex:self.currentRunFrame]];
            self.currentRunFrame = self.currentRunFrame + 1;
            if (self.currentRunFrame == 8) self.currentRunFrame = 1;
        }
        
    }
}


-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    
    //Reset to standing animation
    if (![self.bond hasActions] && !self.isBondCrouching) {
        if (!self.isBondFacingLeft){
            [self.bond setTexture:[SKTexture textureWithImageNamed:@"stand"]];
        } else {
            [self.bond setTexture:[SKTexture textureWithImageNamed:@"leftstand"]];
        }
        self.currentRunFrame = 1;
    }
    
    //Check if the level has been beaten by reaching the right side of the screen
    if (self.bond.position.x > 1000 && !self.hasBeatenLevel) {
        //Remove bond from the level so you don't get two on the screen transition
        [self.bond removeFromParent];
        self.hasBeatenLevel = YES;
        DrNoScene *drNo = [DrNoScene sceneWithSize:CGSizeMake(1024, 768)];
        SKTransition *reveal = [SKTransition pushWithDirection:SKTransitionDirectionLeft duration:1.0];
        //  Optionally, insert code to configure the new scene.
        [self.view presentScene:drNo transition:reveal];
        
    }
    
//    if (self.bond.position.y < 95) {
//        [self.bond setPosition:CGPointMake(self.bond.position.x, 100)];
//    }
    
    //reset the zRotation is is gets off kilter
    if (self.bond.zRotation != 0) {
        self.bond.zRotation = 0;
    }
    
    
}

//Run the jump animation on the bond sprite
-(void)jump
{
    self.isBondCrouching = NO;
    self.bond.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.bond.frame.size];
    SKAction *jumpAnimation;
    if (!self.isBondFacingLeft) {
        jumpAnimation = [SKAction animateWithTextures:self.bondJumpFrames timePerFrame:.1];
    } else {
        jumpAnimation = [SKAction animateWithTextures:self.leftBondJumpFrames timePerFrame:.1];
    }
    SKAction *jump = [SKAction moveByX:0 y:100 duration:.25];
    SKAction *drop = [SKAction moveByX:0 y:-100 duration:.25];
    SKAction *seq = [SKAction sequence:@[jump,drop]];
    SKAction *group = [SKAction group:@[jumpAnimation,seq]];
    [self.bond runAction:group];
    
    
}

//Run the crouch animation on the bond sprite
-(void)crouch
{
    self.isBondCrouching = YES;
    if (!self.isBondFacingLeft) {
        SKAction *crouchAnimation = [SKAction animateWithTextures:self.bondCrouchFrames timePerFrame:.1 resize:NO restore:NO];
        [self.bond runAction:crouchAnimation];
    } else {
        SKAction *crouchAnimation = [SKAction animateWithTextures:self.leftBondCrouchFrames timePerFrame:.1 resize:NO restore:NO];
        [self.bond runAction:crouchAnimation];
    }
    
    //Kind of hacky, but update the physics body to the right height
    self.bond.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.bond.frame.size.width, self.bond.frame.size.height / 2) center:CGPointMake(0, -15.625)];
    
    //This resets the physics body if bond is facing left. I'm flipping the xscale which messes up the physics body. Hack, but works.
//    if (self.bond.xScale == -1.0){
//        //Kind of hacky, but update the physics body to the right height
//        self.bond.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.bond.frame.size.width, self.bond.frame.size.height / 2) center:CGPointMake(0, 15.625)];
//    }

    
}

-(void)moveBondToRightSideOfScreen
{
    [self.bond setPosition:CGPointMake(999, self.bond.position.y)];
    self.isBondFacingLeft = YES;
}

-(void)dealloc {
    [self.view removeGestureRecognizer:self.swipeUpGesture];
}

@end
