//
//  DrNoScene.m
//  Bond-C2C-iOS
//
//  Created by Timothy Hart on 4/2/14.
//  Copyright (c) 2014 Timothy Hart. All rights reserved.
//

#import "DrNoScene.h"
#import "MyScene.h"
#import "SkyfallScene.h"

@implementation DrNoScene

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        self.backgroundColor = [SKColor colorWithRed:0.15 green:0.15 blue:0.15 alpha:1.0];
        
        SKSpriteNode *poster = [SKSpriteNode spriteNodeWithImageNamed:@"drnoposter"];
        poster.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height /2);
        [self addChild:poster];
        
        self.bond = [SKSpriteNode spriteNodeWithImageNamed:@"stand"];
        self.bond.name = @"bond";
        self.bond.position = CGPointMake(25, 100);
        [self addChild:self.bond];
        
        self.hasBeatenLevel = NO;
        
        //Set up the run frames
        NSMutableArray *runFrames = [NSMutableArray array];
        SKTextureAtlas *bondAnimatedAtlas = [SKTextureAtlas atlasNamed:@"bondImages"];
        for (int i = 1; i <= 8; i++) {
            NSString *textureName = [NSString stringWithFormat:@"run%d", i];
            SKTexture *temp = [bondAnimatedAtlas textureNamed:textureName];
            [runFrames addObject:temp];
        }
        self.bondRunFrames = runFrames;
        self.currentRunFrame = 1;
        
        //Setup the jump frames
        NSMutableArray *jumpFrames = [NSMutableArray array];
        for (int i = 1; i <= 3; i++){
            NSString *textureName = [NSString stringWithFormat:@"jump%d", i];
            SKTexture *temp = [bondAnimatedAtlas textureNamed:textureName];
            [jumpFrames addObject:temp];
        }
        self.bondJumpFrames = jumpFrames;
        self.currentJumpFrame = 1;
    }
    return self;
}

-(void)didMoveToView:(SKView *)view {
    self.swipeUpGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(jump)];
    self.swipeUpGesture.direction = UISwipeGestureRecognizerDirectionUp;
    [view addGestureRecognizer:self.swipeUpGesture];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        if (location.x > 512) {
            SKSpriteNode *bond = (SKSpriteNode *)[self childNodeWithName:@"bond"];
            bond.xScale = 1.0;
            [bond runAction:[SKAction moveByX:20 y:0 duration:.2]];
            [bond setTexture:[self.bondRunFrames objectAtIndex:self.currentRunFrame]];
            self.currentRunFrame = self.currentRunFrame + 1;
            if (self.currentRunFrame == 8) self.currentRunFrame = 1;
        } else if (location.x < 512) {
            SKSpriteNode *bond = (SKSpriteNode *)[self childNodeWithName:@"bond"];
            bond.xScale = -1.0;
            [bond runAction:[SKAction moveByX:-20 y:0 duration:.2]];
            [bond setTexture:[self.bondRunFrames objectAtIndex:self.currentRunFrame]];
            self.currentRunFrame = self.currentRunFrame + 1;
            if (self.currentRunFrame == 8) self.currentRunFrame = 1;
        }
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    
    //Reset to standing animation
    if (![self.bond hasActions]) {
        [self.bond setTexture:[SKTexture textureWithImageNamed:@"stand"]];
        self.currentRunFrame = 1;
    }
    
    
    //Check if the level has been beaten by reaching the right side of the screen
    if (self.bond.position.x > 1000 && !self.hasBeatenLevel) {
        //Remove bond from the level so you don't get two on the screen transition
        [self.bond removeFromParent];
        self.hasBeatenLevel = YES;
        //TODO Insert next level here
        SkyfallScene *skyfall = [SkyfallScene sceneWithSize:CGSizeMake(1024, 768)];
        SKTransition *reveal = [SKTransition pushWithDirection:SKTransitionDirectionLeft duration:1.0];
        //  Optionally, insert code to configure the new scene.
        [self.view presentScene:skyfall transition:reveal];
        
    } else if (self.bond.position.x < 5 && !self.hasBeatenLevel) {
        //Remove bond from the level so you don't get two on the screen transition
        [self.bond removeFromParent];
        self.hasBeatenLevel = YES;
        MyScene *titleScreen = [MyScene sceneWithSize:CGSizeMake(1024, 768)];
        SKTransition *reveal = [SKTransition pushWithDirection:SKTransitionDirectionRight duration:1.0];
        [titleScreen moveBondToRightSideOfScreen];
        [titleScreen.bond setXScale:-1.0];
        [self.view presentScene:titleScreen transition:reveal];

    }
    
    
}

//Run the jump animation on the bond sprite
-(void)jump
{
    SKAction *jumpAnimation = [SKAction animateWithTextures:self.bondJumpFrames timePerFrame:.1];
    SKAction *jump = [SKAction moveByX:0 y:100 duration:.25];
    SKAction *drop = [SKAction moveByX:0 y:-100 duration:.25];
    SKAction *seq = [SKAction sequence:@[jump,drop]];
    SKAction *group = [SKAction group:@[jumpAnimation,seq]];
    [self.bond runAction:group];
    
    
}

-(void)moveBondToRightSideOfScreen
{
    [self.bond setPosition:CGPointMake(999, self.bond.position.y)];
}

-(void)dealloc {
    [self.view removeGestureRecognizer:self.swipeUpGesture];
}

@end
