//
//  DrNoScene.h
//  Bond-C2C-iOS
//
//  Created by Timothy Hart on 4/2/14.
//  Copyright (c) 2014 Timothy Hart. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface DrNoScene : SKScene <UIGestureRecognizerDelegate>

@property (nonatomic) SKSpriteNode *bond;
@property (nonatomic, strong) NSArray *bondRunFrames;
@property int currentRunFrame;
@property (nonatomic, strong) NSArray *bondJumpFrames;
@property int currentJumpFrame;

@property BOOL hasBeatenLevel;


@property (nonatomic) UISwipeGestureRecognizer *swipeUpGesture;

-(void)moveBondToRightSideOfScreen;

@end
