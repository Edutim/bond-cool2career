//
//  AppDelegate.h
//  Bond-C2C-iOS
//
//  Created by Timothy Hart on 4/2/14.
//  Copyright (c) 2014 Timothy Hart. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
